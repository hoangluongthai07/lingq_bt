﻿// đọc file mock-data.json trong project để lấy dữ liệu
using BaiTap_LingQ;
using System.Text.Json;

var employeesJson = File.ReadAllText("mock-data.json");
List<Employee> employees = JsonSerializer.Deserialize<List<Employee>>(employeesJson);
// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

//Lấy ra danh sách các FirstName của tất cả các nhân viên.
var employeeName = employees.Select(x => x.FirstName);
Console.WriteLine("First Names of all employees:");
foreach (var name in employeeName)
{
    Console.WriteLine(name);
}

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
var highSalaryWEmployees = employees.Where(x => x.Salary > 50000);

//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
var maleEmployees = employees.Where(x => x.Gender == "Male")
                                                    .OrderBy(x => x.FirstName);

//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
var employeesAreManagerAndInIndonesia = employees.Where(x => x.Country == "Indonesia" && x.Jobtitle == "Manager");

//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
var emailEmployees = employees.OrderByDescending(x => x.LastName)
                                              .Select(x => x.Email);
//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
var employee1 = employees.Where(x => x.StartDay < DateTime.Parse("2022/01/01") && x.Salary > 60000);

//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
var employeesNotHavePostalCode = employees.Where(x => string.IsNullOrEmpty(x.PostalCode))
                                                                 .OrderBy(x => x.LastName);

//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
var employee2 = employees.Where(x => x.FirstName.Equals(x.FirstName.ToUpper()) && x.LastName.Equals(x.LastName.ToUpper()))
                                                .OrderByDescending(x => x.Id);

//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
var employee3 = employees.Where(x => x.Salary >= 50_000 && x.Salary <= 70_000)
                                                .OrderBy(x => x.Salary);

//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
var employe4 = employees.Where(x => x.FirstName.ToLower().Contains('a'))
                                               .OrderByDescending(x => x.LastName);
Console.WriteLine();
